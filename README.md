#Social media influence
Social media influence is a marketing term that describes an individual’s ability to affect other people's thinking in a social online community.

```bash
    git clone https://gitlab.com/othman_akka/social_media_influence.git
```
then create your own virtual environment by:

```bash
    python3 -m venv venv
```
```bash
    pip install -r requirements.txt
```
if webdriver_manager library not install, you can install that by :

```bash
    pip install webdriver_manager --user 
```
then you can spear a programm by :

```bash
    python main.py
```




