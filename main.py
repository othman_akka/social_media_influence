from influence_functions import Functions
from instagram import Instagram
from linkedin import LinkedIn
from facebook import Facebook
from twitter import Twitter

# https://www.facebook.com/pg/vijesti.me/

def Facebook_():
    ## NP : format of date of posts whether 1m, 1h, 1d, 1 March or 1 January 2019
    dict_months_facebook = {'January': 1, 'February': 2, 'March': 3, 'April': 4, 'May': 5, 'June': 6, 'July': 7,'August': 8, 'September': 9, 'October': 10, 'November': 11, 'December': 12}

    facebook = Facebook(email='ahmed.hilalez@gmail.com', password='jijiJIJI00',url_profile='https://www.facebook.com/setoo99/', date='11 December 2019',dict_months=dict_months_facebook, browser='Chrome')
    facebook.Login()
    facebook.Scraping()

    influece = Functions()
    print("Engagement normal of facebook :",influece.Influence_facebook(filename='features_facebook.csv', start_time='15 June', end_time='1 April'))
    print("Engagement with coefficients of facebook :",influece.Influence_facebook_coefficient(filename='features_facebook.csv', start_time='15 June',end_time='1 April'))

def Instagram_():
    ## NP : format of date of posts whether 2 MINUTES AGO, 4 HOURS AGO, 2 DAYS AGO, MARCH 3 or DECEMBER 1, 2019
    dict_months_instagram = {'JANUARY': 1, 'FEBRUARY': 2, 'MARCH': 3, 'APRIL': 4, 'MAY': 5, 'JUNE': 6, 'JULY': 7,'AUGUST': 8, 'SEPTEMBER': 9, 'OCTOBER': 10, 'NOVEMBER': 11, 'DECEMBER': 12}

    instagram = Instagram(email='ahmed.hilalez@gmail.com', password='jijiJIJI00',url_profile='https://www.instagram.com/cristiano/', date='JANUARY 23',dict_months=dict_months_instagram, browser='Chrome')
    instagram.Login()
    instagram.Scraping()

    influece = Functions()
    print("Engagement normal of Instagram :",influece.Influence_Instagram(filename='features_instagram.csv', start_time='JUNE 16', end_time='MAY 17'))
    print("Engagement with coefficients of Instagram :",influece.Influence_instagram_coefficient(filename='features_instagram.csv', start_time='JUNE 16',end_time='MAY 17'))

def LinkedIn_():
    ## NP : format of date of posts whether 2 minutes ago, 2 hours ago, 1 day ago,2 weeks ago, 2 months ago, 2 years ago
    linkedin = LinkedIn(email='ahmed.hilalez@gmail.com', password='jijiJIJI00',url_profile='https://www.linkedin.com/in/williamhgates/', date='2 years ago', browser='Chrome')
    linkedin.Login()
    linkedin.Scraping()

    influece = Functions()
    print("Engagement normal of LinkedIn :",influece.Influence_LinkedIn(filename='features_linkedin.csv', start_time='6 days ago',end_time='7 months ago'))
    print("Engagement with coefficients of LinkedIn :",influece.Influence_LinkedIn_coefficient(filename='features_linkedin.csv', start_time='6 days ago',end_time='7 months ago'))

def Twitter_():
    ## NP : format of date of posts whether Aug 18, 2020 or Dec 18, 2019
    dict_months_twitter = {'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4, 'May': 5, 'Jun': 6, 'Jul': 7, 'Aug': 8, 'Sep': 9,'Oct': 10, 'Nov': 11, 'Dec': 12}

    twitter = Twitter(email='ahmed.hilalez@gmail.com', password = 'jijiJIJI00',url_profile = 'https://twitter.com/YBelhaissi', date = 'Jun 16, 2020',dict_months = dict_months_twitter, browser = 'Chrome')
    twitter.Login()
    twitter.Scraping()

    influece = Functions()
    print("Engagement normal of Twitter :",influece.Influence_Twitter(filename='features_twitter.csv', start_time='Aug 18, 2020',end_time='Aug 17, 2020'))
    print("Engagement with coefficients of Twitter :",influece.Influence_Twitter_coefficient(filename='features_twitter.csv', start_time='Aug 18, 2020',end_time='Aug 17, 2020'))

if __name__ == '__main__':

    Facebook_()
    #Instagram_()
    #LinkedIn_()
    #Twitter_()






