from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium import webdriver
from decimal import Decimal
import pandas as pd
import time
import re

class LinkedIn():
    def __init__(self, email, password, url_profile, date, browser='Chrome'):
        LOGIN_URL = 'https://www.linkedin.com/login'

        self.email = email
        self.password = password
        self.url_profile = url_profile
        self.date = date
        self.post = None

        self.followers_count = []
        self.likes_count = []
        self.comments_count = []
        self.date_posts = []

        self.follower_count = 0

        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        chrome_options.add_experimental_option("prefs", prefs)

        if browser == 'Chrome':
            self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=chrome_options)
        elif browser == 'Firefox':
            self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        self.driver.get(LOGIN_URL)
        time.sleep(1)

    def Login(self):
        email_element = self.driver.find_element_by_id('username')
        email_element.send_keys(self.email)

        password_element = self.driver.find_element_by_id('password')
        password_element.send_keys(self.password)

        login_button = self.driver.find_element_by_xpath("//div[@class='login__form_action_container ']")
        login_button.click()

        time.sleep(2)

    def Scraping(self):
        self.driver.get(self.url_profile)
        time.sleep(2)

        # scroll
        self.driver.execute_script("window.scrollBy(0, 900);")
        time.sleep(7)

        profile_info = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@class='profile-detail']")))
        activity = profile_info.find_element_by_xpath(".//div[5]")

        followers_count = activity.find_element_by_xpath(".//span[1]")
        self.follower_count = self.clean(followers_count.text.split()[0])

        see_all = activity.find_element_by_xpath(".//span[@class='pv-profile-section__section-info']")
        self.driver.implicitly_wait(10)
        ActionChains(self.driver).move_to_element(see_all).click(see_all).perform()

        # scroll
        for i in range(30):
            self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            time.sleep(5)

        try:
            posts = self.driver.find_element_by_xpath("//div[@id='voyager-feed']")

            for post in posts.find_elements_by_xpath(".//div[@class='occludable-update ember-view']"):

                self.like_count = 0
                self.comment_count = 0

                date = post.find_element_by_xpath(".//span[@class='feed-shared-actor__sub-description t-12 t-black--light t-normal']")
                date = date.text.split('\n')[1]

                if self.duration(date):
                    self.post = post.find_element_by_xpath(".//div[@class='social-details-social-activity ember-view']/ul")

                    if self.check_exists_by_xpath(".//span[@class='v-align-middle social-details-social-counts__reactions-count']"):
                        likes_count = self.post.find_element_by_xpath(".//span[@class='v-align-middle social-details-social-counts__reactions-count']")
                        self.like_count = self.clean(likes_count.text)
                        print("likes_count ", self.like_count)

                    if self.check_exists_by_xpath(".//span[@class='v-align-middle']"):
                        comments_count = post.find_element_by_xpath(".//span[@class='v-align-middle']")
                        if re.search("Views", self.clean(comments_count.text)):
                            self.comment_count = 0
                        else :
                            self.comment_count = self.clean(comments_count.text)
                            print("comments_count ", self.comment_count)

                    self.likes_count.append(self.like_count)
                    self.comments_count.append(self.comment_count)
                    self.followers_count.append(self.follower_count)
                    self.date_posts.append(date)
        except:
            print("Loading took too much time!")

        # dictionary of lists
        dictinnary = {'date_posts': self.date_posts, 'likes_count': self.likes_count,'comments_count': self.comments_count, 'followers_count': self.followers_count}

        df = pd.DataFrame(dictinnary)

        # saving the dataframe
        df.to_csv('features_linkedin.csv', index=False, sep=',', encoding='utf-8')
        self.driver.close()

    def duration(self, date):
        date_own = self.split_word(self.date)
        date_scrapy = self.split_word(date)

        if str(date_own[0]) == "minute":
            if str(date_scrapy[0]) == "minute":
                if int(date_scrapy[1]) <= int(date_own[1]):
                    return True
                else:
                    return False

        elif str(date_own[0]) == "hour":
            if str(date_scrapy[0]) == "minute":
                return True
            elif str(date_scrapy[0]) == "hour":
                if int(date_scrapy[1]) <= int(date_own[1]):
                    return True
                else:
                    return False

        elif str(date_own[0]) == "day":
            # minutes or hours
            if str(date_scrapy[0]) == "minute" or str(date_scrapy[0]) == "hour":
                return True
            elif str(date_scrapy[0]) == "day":
                # day
                if int(date_scrapy[1]) <= int(date_own[1]):
                    return True
                else:
                    return False

        elif str(date_own[0]) == "week":
            # minutes, hours or day
            if str(date_scrapy[0]) == "minute" or str(date_scrapy[0]) == "hour" or str(date_scrapy[0]) == "day":
                return True
            elif str(date_scrapy[0]) == "week":
                # week
                if int(date_scrapy[1]) <= int(date_own[1]):
                    return True
                else:
                    return False

        elif str(date_own[0]) == "month":
            # minutes, hours, day or week
            if str(date_scrapy[0]) == "minute" or str(date_scrapy[0]) == "hour" or str(date_scrapy[0]) == "day" or str(date_scrapy[0]) == "week":
                return True
            elif str(date_scrapy[0]) == "month":
                # month
                if int(date_scrapy[1]) <= int(date_own[1]):
                    return True
                else:
                    return False

        elif str(date_own[0]) == "year":
            # minutes, hours, day, week or month
            if str(date_scrapy[0]) == "minute" or str(date_scrapy[0]) == "hour" or str(date_scrapy[0]) == "day" or str(date_scrapy[0]) == "week" or str(date_scrapy[0]) == "month":
                return True
            elif str(date_scrapy[0]) == "year":
                # year
                if int(date_scrapy[1]) <= int(date_own[1]):
                    return True
                else:
                    return False


    def split_word(self, word):
        #day of linkedin
        if re.search("minute", word):
            return ['minute',word.split()[0]]
        elif re.search("hour", word):
            return ['hour', word.split()[0]]
        elif re.search("day", word):
            return ['day', word.split()[0]]
        elif re.search("week", word):
            return ['week', word.split()[0]]
        elif re.search("month", word):
            return ['month', word.split()[0]]
        elif re.search("year", word):
            return ['year', word.split()[0]]

    def clean(self, string):
        if re.search(",",string):
            string = string.replace(",","")
        if re.search("Comments",string):
            string = string.replace("Comments","")
        if re.search("Views",string):
            string = string.replace("Views","")
        return string

    def check_exists_by_xpath(self, xpath):
        try:
            self.post.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True







