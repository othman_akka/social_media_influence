from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium import webdriver
from decimal import Decimal
import pandas as pd
import time
import re

class Instagram():
    def __init__(self, email, password, url_profile, date, dict_months, browser='Chrome'):
        LOGIN_URL = 'https://www.instagram.com/accounts/login/'

        self.email = email
        self.password = password
        self.url_profile = url_profile
        self.date = date
        self.dict_months = dict_months

        self.followers_count = []
        self.likes_count = []
        self.comments_count = []
        self.date_posts = []

        self.follower_count = 0

        self.chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        self.chrome_options.add_experimental_option("prefs", prefs)
        mobile_emulation = {"deviceName": "Nexus 5"}
        self.chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)


        if browser == 'Chrome':
            self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=self.chrome_options)
        elif browser == 'Firefox':
            self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        self.driver.get(LOGIN_URL)
        time.sleep(1)

    def Login(self):
        button_facebook = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located((By.XPATH, "//button[@class='sqdOP  L3NKy   y3zKF     ']")))
        self.driver.implicitly_wait(10)
        ActionChains(self.driver).move_to_element(button_facebook).click(button_facebook).perform()

        email_element = self.driver.find_element_by_name('email')
        email_element.send_keys(self.email)

        password_element = self.driver.find_element_by_name('pass')
        password_element.send_keys(self.password)

        login_button = self.driver.find_element_by_name('login')
        login_button.click()

        time.sleep(5)
        if self.check_exists_by_xpath("//button[contains(text(),'Cancel')]"):
            self.driver.find_element_by_xpath("//button[contains(text(),'Cancel')]").click()


    def Scraping(self):
        self.driver.get(self.url_profile)
        if self.check_exists_by_xpath("//div[@class='v9tJq  VfzDr']"):
            followers_count = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@class='v9tJq  VfzDr']/ul/li[2]")))
            self.follower_count = self.format(followers_count.text.split()[0])

            list_links = []

            try:
                for i in range(30):
                    self.driver.implicitly_wait(10)
                    posts = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@class='v9tJq  VfzDr']")))
                    posts = posts.find_element_by_xpath(".//div[@class=' _2z6nI']/article/div[1]/div")
                    cells = posts.find_elements_by_xpath(".//div[@class='Nnq7C weEfm']")

                    for cell in cells:
                        posts = cell.find_elements_by_xpath(".//div[@class='v1Nh3 kIKUG  _bz0w']")
                        for post in posts:
                            link = post.find_element_by_xpath(".//a").get_attribute('href')
                            list_links.append(link)

                    self.driver.execute_script("window.scrollBy(0, 500);")
                    time.sleep(4)

                list_links = set(list_links)

                for link in list_links:
                    self.like_count = 0
                    self.comment_count = 0

                    self.driver.get(link)
                    time.sleep(5)

                    date = self.driver.find_element_by_xpath("//div[@class='k_Q0X NnvRN']/a")
                    if self.duration(date.text):
                        if self.check_exists_by_xpath("//div[@class='HbPOm _9Ytll']"):
                            likes_count = self.driver.find_element_by_xpath("//div[@class='HbPOm _9Ytll']")
                            print("likes1 :", self.clean(likes_count.text))
                            self.like_count = self.clean(likes_count.text)

                        elif self.check_exists_by_xpath("//a[@class='zV_Nj']"):
                            likes_count = self.driver.find_element_by_xpath("//a[@class='zV_Nj']/span")
                            print("likes2 :", self.clean(likes_count.text))
                            self.like_count = self.clean(likes_count.text)

                        if self.check_exists_by_xpath("//a[@class='r8ZrO']"):
                            comments_count = self.driver.find_element_by_xpath("//a[@class='r8ZrO']")
                            print("comments :", self.clean(comments_count.text))
                            self.comment_count = self.clean(comments_count.text)

                        self.likes_count.append(self.like_count)
                        self.comments_count.append(self.comment_count)
                        self.followers_count.append(self.follower_count)
                        self.date_posts.append(date.text)
            except:
                print("Loading took too much time!")

        # dictionary of lists
        dictinnary = {'date_posts': self.date_posts, 'likes_count': self.likes_count,'comments_count': self.comments_count,'followers_count': self.followers_count}

        df = pd.DataFrame(dictinnary)

        # saving the dataframe
        df.to_csv('features_instagram.csv', index=False, sep=',', encoding='utf-8')
        self.driver.close()

    def duration(self, date):
        date_own = self.split_word(self.date)
        date_scrapy = self.split_word(date)

        if str(date_own[0]) == "MINUTE":
            if str(date_scrapy[0]) == "MINUTE":
                if int(date_scrapy[1]) <= int(date_own[1]):
                    return True
                else:
                    return False

        elif str(date_own[0]) == "HOUR":
            if str(date_scrapy[0]) == "MINUTE":
                return True
            elif str(date_scrapy[0]) == "HOUR":
                if int(date_scrapy[1]) <= int(date_own[1]):
                    return True
                else:
                    return False

        elif str(date_own[0]) == "DAY":
            # minutes or hours
            if str(date_scrapy[0]) == "MINUTE" or str(date_scrapy[0]) == "HOUR":
                return True
            elif str(date_scrapy[0]) == "DAY":
                # day
                if int(date_scrapy[1]) <= int(date_own[1]):
                    print(date_scrapy)
                    return True
                else:
                    return False

        elif str(date_own[0]) == "MONTH":
            # minutes hours or days
            if str(date_scrapy[0]) == "MINUTE" or str(date_scrapy[0]) == "HOUR" or str(date_scrapy[0]) == "DAY":
                return True
            # month
            elif str(date_scrapy[0]) == "MONTH":
                # day
                if int(self.dict_months[date_own[2]]) == int(self.dict_months[date_scrapy[2]]) :
                    if int(date_own[1]) <= int(date_scrapy[1]):
                        return True
                    else :
                        return False
                elif self.dict_months[date_own[2]] < self.dict_months[date_scrapy[2]]:
                    return True
                else:
                    return False
            else:
                return False

        elif str(date_own[0]) == "YEAR":
            # minutes, hours, days or months
            if str(date_scrapy[0]) == "MINUTE" or str(date_scrapy[0]) == "HOUR" or str(date_scrapy[0]) == "DAY" or str(date_scrapy[0]) == "MONTH":
                return True
            # year
            elif int(date_own[3]) == int(date_scrapy[3]):
                #month
                if int(self.dict_months[date_own[2]]) < int(self.dict_months[date_scrapy[2]]):
                    return True
                elif int(self.dict_months[date_own[2]]) == int(self.dict_months[date_scrapy[2]]):
                    #day
                    if int(date_own[1]) <= int(date_scrapy[1]):
                        return True
                    else:
                        return Flase
                else:
                    return False
            elif int(date_own[3]) < int(date_scrapy[3]):
                return True
            else:
                return False

    def split_word(self, word):
        #day of instagram
        if re.search("MINUTE", word):
            return ['MINUTE',word.split()[1]]
        elif re.search("HOUR", word):
            return ['HOUR', word.split()[1]]
        elif re.search("DAY", word):
            return ['DAY', word.split()[1]]
            # index day month
        elif word.split()[0] in list(self.dict_months.keys()) and len(word.split()) == 2:
            return ['MONTH', word.split()[1].replace(",",""), word.split()[0]]
            # index day month year
        elif word.split()[0] in list(self.dict_months.keys()) and len(word.split()) == 3:
            return ['YEAR', word.split()[1].replace(",",""), word.split()[0], word.split()[2]]

    def format(self ,word):
        if re.search("k", word):
            return Decimal(word.replace('k', ''))*1000
        elif re.search("m", word):
            return Decimal(word.replace('m', ''))*1000000
        else:
            return word

    def clean(self, string):
        if re.search(",",string):
            string = string.replace(",","")
        if re.search("View all",string):
            string = string.replace("View all","")
        if re.search("comments",string):
            string = string.replace("comments","")
        if re.search("views",string):
            string = string.replace("views","")
        return string

    def check_exists_by_xpath(self, xpath):
        try:
            self.driver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True


