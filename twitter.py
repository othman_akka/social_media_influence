from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium import webdriver
from decimal import Decimal
import pandas as pd
import time
import re

class Twitter():
    def __init__(self, email, password, url_profile, date, dict_months, browser='Chrome'):
        LOGIN_URL = 'https://twitter.com/login'

        self.email = email
        self.password = password
        self.url_profile = url_profile
        self.date = date
        self.dict_months = dict_months
        self.post = None

        self.followers_count = []
        self.likes_count = []
        self.comments_count = []
        self.date_posts = []

        self.follower_count = 0

        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        chrome_options.add_experimental_option("prefs", prefs)

        if browser == 'Chrome':
            self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=chrome_options)
        elif browser == 'Firefox':
            self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        self.driver.get(LOGIN_URL)
        time.sleep(1)

    def Login(self):
        email_element = self.driver.find_element_by_name('session[username_or_email]')
        email_element.send_keys(self.email)

        password_element = self.driver.find_element_by_name('session[password]')
        password_element.send_keys(self.password)

        login_button = self.driver.find_element_by_xpath("//div[@class='css-901oao r-1awozwy r-jwli3a r-6koalj r-18u37iz r-16y2uox r-1qd0xha r-a023e6 r-vw2c0b r-1777fci r-eljoum r-dnmrzs r-bcqeeo r-q4m81j r-qvutc0']")
        login_button.click()

        time.sleep(2)

    def Scraping(self):
        self.driver.get(self.url_profile)
        time.sleep(2)

        try:
            profile_info = WebDriverWait(self.driver, 100).until(
                EC.presence_of_element_located((By.XPATH, "//div[@class='css-1dbjc4n r-16y2uox']")))

            followers_count = profile_info.find_element_by_xpath(".//div[@class='css-1dbjc4n r-18u37iz r-1w6e6rj']/div[2]")

            followers_count = self.clean(followers_count.text.split()[0])
            self.follower_count = self.format(followers_count)

            posts_links = []

            for i in range(100):
                all = self.driver.find_element_by_xpath("//div[@class='css-1dbjc4n r-1jgb5lz r-1ye8kvj r-13qz1uu']/div[2]")
                all = all.find_element_by_xpath(".//section[@class='css-1dbjc4n']")
                posts = all.find_elements_by_xpath(".//div[@class='css-1dbjc4n']/div/div")

                self.driver.execute_script("window.scrollBy(0, 500);")
                time.sleep(7)

                for post in posts:
                    try:
                        self.post = post
                        if self.check_exists_by_xpath(".//div[@class='css-1dbjc4n r-1d09ksm r-18u37iz r-1wbh5a2']"):
                            link = post.find_element_by_xpath(".//div[@class='css-1dbjc4n r-1d09ksm r-18u37iz r-1wbh5a2']/a")
                            posts_links.append(link.get_attribute('href'))
                    except:
                        continue

            posts_links = set(posts_links)

            for link in posts_links:
                self.driver.get(link)
                time.sleep(3)

                self.like_count = 0
                self.comment_count = 0

                post = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@class='css-1dbjc4n r-my5ep6 r-qklmqi r-1adg3ll']")))

                date = post.find_element_by_xpath(".//div[@class='css-1dbjc4n r-vpgt9t']")
                date = date.find_element_by_xpath(".//span[@class='css-901oao css-16my406 r-1qd0xha r-ad9z0x r-bcqeeo r-qvutc0']")
                date = date.text.split(' · ')[1]

                if self.duration(date) :

                    self.post = post
                    if self.check_exists_by_xpath(".//div[@class='css-1dbjc4n r-1gkumvb r-1efd50x r-5kkj8d r-18u37iz r-tzz3ar r-ou255f r-9qu9m4']"):
                        like_comment = post.find_element_by_xpath(".//div[@class='css-1dbjc4n r-1gkumvb r-1efd50x r-5kkj8d r-18u37iz r-tzz3ar r-ou255f r-9qu9m4']")
                        self.post = like_comment

                        if self.check_exists_by_xpath(".//div[1]"):
                            if re.search('comment',like_comment.find_element_by_xpath(".//div[1]").text):
                                comment_count = like_comment.find_element_by_xpath(".//div[1]").text.split("\n")[0]
                                comment_count = self.clean(comment_count)
                                self.comment_count = self.format(comment_count)
                                print("comment_count :", self.comment_count)
                            elif re.search('Likes',like_comment.find_element_by_xpath(".//div[1]").text):
                                like_count = like_comment.find_element_by_xpath(".//div[1]").text.split("\n")[0]
                                like_count = self.clean(like_count)
                                self.like_count = self.format(like_count)
                                print("like_count :", self.like_count)

                        if self.check_exists_by_xpath(".//div[2]"):
                            like_count = like_comment.find_element_by_xpath(".//div[2]").text.split("\n")[0]
                            like_count = self.clean(like_count)
                            self.like_count = self.format(like_count)
                            print("like_count :", self.like_count)

                    self.likes_count.append(self.like_count)
                    self.comments_count.append(self.comment_count)
                    self.followers_count.append(self.follower_count)
                    self.date_posts.append(date)
        except:
            print("Loading took too much time!")

        # dictionary of lists
        dictinnary = {'date_posts': self.date_posts, 'likes_count': self.likes_count,'comments_count': self.comments_count, 'followers_count': self.followers_count}

        df = pd.DataFrame(dictinnary)

        # saving the dataframe
        df.to_csv('features_twitter.csv', index=False, sep=',', encoding='utf-8')
        self.driver.close()

    def duration(self, date):
        date_own = self.clean(self.date).split(" ")
        date_scrapy = self.clean(date).split(" ")

        if int(date_own[2]) == int(date_scrapy[2]):
            if int(self.dict_months[date_own[0]]) == int(self.dict_months[date_scrapy[0]]) :
                if int(date_own[1]) <= int(date_scrapy[1]):
                    return True
                else :
                    return False
            elif int(self.dict_months[date_own[0]]) < int(self.dict_months[date_scrapy[0]]):
                return True
            else:
                return False
        elif int(date_own[2]) < int(date_scrapy[2]):
            return True
        else:
            return False

    def format(self ,word):
        if re.search("K", word):
            return Decimal(word.replace('K', ''))*1000
        elif re.search("M", word):
            return Decimal(word.replace('M', ''))*1000000
        else:
            return word

    def clean(self, string):
        if re.search(",",string):
            string = string.replace(",","")
        if re.search("View all",string):
            string = string.replace("View all","")
        if re.search("comments",string):
            string = string.replace("comments","")
        if re.search("views",string):
            string = string.replace("views","")
        return string

    def check_exists_by_xpath(self, xpath):
        try:
            self.post.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True