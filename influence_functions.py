from decimal import Decimal
from csv import DictReader
from csv import reader

class Functions():
    def __init__(self):
        self.followers_count = 0
        self.list_features = []

        self.like_count = 0
        self.comment_count = 0
        self.share_count = 0
        self.influence_post = 0
        self.average_influence_posts = 0

    def read_features(self, filename, start_time , end_time):
        list_features = []
        followers_count = 0

        with open(filename, 'r') as file:
            for row in reader(file):
                if len(row) == 4:
                    print("4 format of date ", row[0])
                    if str(row[0]) == start_time or len(list_features) != 0:
                        list_features.append([row[1],row[2]])
                        followers_count = row[3]
                        if str(row[0]) == end_time and str(row[0]) != start_time:
                            break

                elif len(row) == 5:
                    print("5 format of date ", row[0])
                    if str(row[0]) == start_time or len(list_features) != 0:
                        list_features.append([row[1],row[2],row[3]])
                        followers_count = row[4]
                        if str(row[0]) == end_time and str(row[0]) != start_time:
                            break

        return [list_features, followers_count]

    def Influence_facebook(self, filename, start_time , end_time):

        [self.list_features, self.followers_count] = self.read_features(filename, start_time , end_time)

        if len(self.list_features) != 0:
            for liste in self.list_features:
                self.like_count = Decimal(liste[0])
                self.comment_count = Decimal(liste[1])
                self.share_count = Decimal(liste[2])
                self.influence_post = self.influence_post + ((self.like_count + self.comment_count + self.share_count) / Decimal(self.followers_count)) * 100
                self.average_influence_posts = self.influence_post / len(self.list_features)
        else :
            self.average_influence_posts = 0
        return self.average_influence_posts

    def Influence_facebook_coefficient(self, filename, start_time , end_time):

        [self.list_features, self.followers_count] = self.read_features(filename, start_time , end_time)

        if len(self.list_features) != 0:
            for liste in self.list_features:
                self.like_count = Decimal(liste[0])
                self.comment_count = Decimal(liste[1])
                self.share_count = Decimal(liste[2])
                self.influence_post = self.influence_post + ((Decimal(1/51)*self.like_count + Decimal(20/51)*self.comment_count + Decimal(30/51)*self.share_count) / Decimal(self.followers_count)) * 100
                self.average_influence_posts = self.influence_post / len(self.list_features)
        else :
            self.average_influence_posts = 0
        return self.average_influence_posts

    def Influence_Instagram(self, filename, start_time , end_time):

        [self.list_features, self.followers_count] = self.read_features(filename, start_time , end_time)
        if len(self.list_features) != 0:
            for liste in self.list_features:
                self.like_count = Decimal(liste[0])
                self.comment_count = Decimal(liste[1])
                self.influence_post = self.influence_post + ((self.like_count + self.comment_count) / Decimal(self.followers_count)) * 100
                self.average_influence_posts = self.influence_post / len(self.list_features)
        else :
            self.average_influence_posts = 0
        return self.average_influence_posts


    def Influence_instagram_coefficient(self, filename, start_time , end_time):

        [self.list_features, self.followers_count] = self.read_features(filename, start_time , end_time)

        if len(self.list_features) != 0:
            for liste in self.list_features:
                self.like_count = Decimal(liste[0])
                self.comment_count = Decimal(liste[1])
                self.influence_post = self.influence_post + ((Decimal(1/21)*self.like_count + Decimal(20/21)*self.comment_count) / Decimal(self.followers_count)) * 100
                self.average_influence_posts = self.influence_post / len(self.list_features)
        else :
            self.average_influence_posts = 0
        return self.average_influence_posts

    def Influence_LinkedIn(self, filename, start_time , end_time):

        [self.list_features, self.followers_count] = self.read_features(filename, start_time , end_time)
        if len(self.list_features) != 0:
            for liste in self.list_features:
                self.like_count = Decimal(liste[0])
                self.comment_count = Decimal(liste[1])
                self.influence_post = self.influence_post + ((self.like_count + self.comment_count) / Decimal(self.followers_count)) * 100
                self.average_influence_posts = self.influence_post / len(self.list_features)
        else :
            self.average_influence_posts = 0
        return self.average_influence_posts


    def Influence_LinkedIn_coefficient(self, filename, start_time , end_time):

        [self.list_features, self.followers_count] = self.read_features(filename, start_time , end_time)

        if len(self.list_features) != 0:
            for liste in self.list_features:
                self.like_count = Decimal(liste[0])
                self.comment_count = Decimal(liste[1])
                self.influence_post = self.influence_post + ((Decimal(1/21)*self.like_count + Decimal(20/21)*self.comment_count) / Decimal(self.followers_count)) * 100
                self.average_influence_posts = self.influence_post / len(self.list_features)
        else :
            self.average_influence_posts = 0
        return self.average_influence_posts

    def Influence_Twitter(self, filename, start_time , end_time):

        [self.list_features, self.followers_count] = self.read_features(filename, start_time , end_time)

        if len(self.list_features) != 0:
            for liste in self.list_features:
                self.like_count = Decimal(liste[0])
                self.comment_count = Decimal(liste[1])
                self.influence_post = self.influence_post + ((self.like_count + self.comment_count) / Decimal(self.followers_count)) * 100
                self.average_influence_posts = self.influence_post / len(self.list_features)
        else :
            self.average_influence_posts = 0
        return self.average_influence_posts


    def Influence_Twitter_coefficient(self, filename, start_time , end_time):

        [self.list_features, self.followers_count] = self.read_features(filename, start_time , end_time)

        if len(self.list_features) != 0:
            for liste in self.list_features:
                self.like_count = Decimal(liste[0])
                self.comment_count = Decimal(liste[1])
                self.influence_post = self.influence_post + ((Decimal(1/21)*self.like_count + Decimal(20/21)*self.comment_count) / Decimal(self.followers_count)) * 100
                self.average_influence_posts = self.influence_post / len(self.list_features)
        else :
            self.average_influence_posts = 0
        return self.average_influence_posts