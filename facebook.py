from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium import webdriver
from decimal import Decimal
import pandas as pd
import time
import re

class Facebook():
    def __init__(self, email, password, url_profile, date, dict_months, browser='Chrome'):
        LOGIN_URL = 'https://www.facebook.com/login.php'

        self.email = email
        self.password = password
        self.url_profile = url_profile
        self.date = date
        self.dict_months = dict_months
        self.post = None

        self.followers_count = []
        self.likes_count = []
        self.comments_count = []
        self.shares_count = []
        self.date_posts = []

        self.follower_count = 0

        self.chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        self.chrome_options.add_experimental_option("prefs", prefs)

        if browser == 'Chrome':
            self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=self.chrome_options)
        elif browser == 'Firefox':
            self.driver = webdriver.Firefox(executable_path=GeckoDriverManager().install())
        self.driver.get(LOGIN_URL)
        time.sleep(1)

    def Login(self):
        email_element = self.driver.find_element_by_id('email')
        email_element.send_keys(self.email)

        password_element = self.driver.find_element_by_id('pass')
        password_element.send_keys(self.password)

        login_button = self.driver.find_element_by_id('loginbutton')
        login_button.click()

        time.sleep(2)

    def Scraping(self):
        self.driver.get(self.url_profile)
        followers_counts = WebDriverWait(self.driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@class='_4-u2 _6590 _3xaf _4-u8']/div[4]")))
        self.follower_count = Decimal(followers_counts.text.split()[0].replace(',', ''))

        self.driver.get(self.url_profile+"posts/")

        # scroll
        for i in range(1, 5):
            self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            time.sleep(5)

        try:
            posts = WebDriverWait(self.driver,100).until(EC.presence_of_element_located((By.XPATH,"//div[@class='_2pie _14i5 _1qkq _1qkx']")))
            posts = posts.find_elements_by_xpath(".//div[@class='_4-u2 _4-u8']")

            for post in posts:

                self.like_count = 0
                self.comment_count = 0
                self.share_count = 0

                date = post.find_element_by_xpath(".//div[@class='_6a _5u5j _6b']")
                date = date.find_element_by_xpath(".//span[@class='l_wk1uwor3g']")
                if self.duration(str(date.text)):
                    self.post = post

                    if self.check_exists_by_xpath(".//div[@class='_66lg']"):
                        like_count = post.find_element_by_xpath(".//div[@class='_66lg']")
                        like_count = like_count.find_element_by_xpath(".//a[@class='_3dlf']")
                        # replace M by 000000 and K by 000
                        self.like_count = self.format(like_count.text.split()[0])
                        print("like_count :", self.like_count)

                    if self.check_exists_by_xpath(".//div[@class='_4vn1']/span[1]"):
                        if re.search("share", str(post.find_element_by_xpath(".//div[@class='_4vn1']/span[1]").text)):
                            share_count = post.find_element_by_xpath(".//div[@class='_4vn1']/span[1]")
                            self.share_count = self.format(share_count.text.split()[0])
                            print("share_count :", self.share_count)
                        else:
                            comment_count = post.find_element_by_xpath(".//div[@class='_4vn1']/span[1]")
                            self.comment_count = self.format(comment_count.text.split()[0])
                            print("comment_count :", self.comment_count)

                    if self.check_exists_by_xpath(".//div[@class='_4vn1']/span[2]"):
                        share_count = post.find_element_by_xpath(".//div[@class='_4vn1']/span[2]")
                        self.share_count = self.format(share_count.text.split()[0])
                        print("share_count :", self.share_count)

                    self.likes_count.append(self.like_count)
                    self.comments_count.append(self.comment_count)
                    self.shares_count.append(self.share_count)
                    self.followers_count.append(self.follower_count)
                    self.date_posts.append(date.text)

        except:
            print("Loading took too much time!")

        # dictionary of lists
        dictinnary = {'date_posts': self.date_posts, 'likes_count': self.likes_count, 'comments_count': self.comments_count, 'shares_count': self.shares_count, 'followers_count': self.followers_count}

        df = pd.DataFrame(dictinnary)

        # saving the dataframe
        df.to_csv('features_facebook.csv', index = False, sep=',', encoding='utf-8')
        self.driver.close()

    def duration(self, date):
        date_own = self.split_word(self.date)
        date_scrapy = self.split_word(date)

        if str(date_own[0]) == "MINUTE":
            if str(date_scrapy[0]) == "MINUTE":
                if int(date_scrapy[1]) <= int(date_own[1]):
                    return True
                else:
                    return False

        elif str(date_own[0]) == "HOUR":
            if str(date_scrapy[0]) == "MINUTE":
                return True
            elif str(date_scrapy[0]) == "HOUR":
                if int(date_scrapy[1]) <= int(date_own[1]):
                    return True
                else:
                    return False

        elif str(date_own[0]) == "DAY":
            # minutes or hours
            if str(date_scrapy[0]) == "MINUTE" or str(date_scrapy[0]) == "HOUR":
                return True
            elif str(date_scrapy[0]) == "DAY":
                # day
                if int(date_scrapy[1]) <= int(date_own[1]):
                    print(date_scrapy)
                    return True
                else:
                    return False

        elif str(date_own[0]) == "MONTH":
            # minutes hours or days
            if str(date_scrapy[0]) == "MINUTE" or str(date_scrapy[0]) == "HOUR" or str(date_scrapy[0]) == "DAY":
                return True
            # month
            elif str(date_scrapy[0]) == "MONTH":
                # day
                if int(self.dict_months[date_own[2]]) == int(self.dict_months[date_scrapy[2]]):
                    if int(date_own[1]) <= int(date_scrapy[1]):
                        return True
                    else:
                        return False
                elif self.dict_months[date_own[2]] < self.dict_months[date_scrapy[2]]:
                    return True
                else:
                    return False
            else:
                return False

        elif str(date_own[0]) == "YEAR":
            # minutes, hours, days or months
            if str(date_scrapy[0]) == "MINUTE" or str(date_scrapy[0]) == "HOUR" or str(date_scrapy[0]) == "DAY" or str(date_scrapy[0]) == "MONTH":
                return True
            # year
            elif int(date_own[3]) == int(date_scrapy[3]):
                # month
                if int(self.dict_months[date_own[2]]) < int(self.dict_months[date_scrapy[2]]):
                    return True
                elif int(self.dict_months[date_own[2]]) == int(self.dict_months[date_scrapy[2]]):
                    # day
                    if int(date_own[1]) <= int(date_scrapy[1]):
                        return True
                    else:
                        return Flase
                else:
                    return False
            elif int(date_own[3]) < int(date_scrapy[3]):
                return True
            else:
                return False

    def split_word(self, word):
        # day of Facebook
        if re.search("m", word) and len(word) <= 3:
            return ['MINUTE', word.replace("m","")]
        elif re.search("h", word) and len(word) <= 3:
            return ['HOUR', word.replace("h","")]
        elif re.search("d", word) and len(word) <= 3:
            return ['DAY', word.replace("d","")]
            # index day month
        elif word.split()[1] in list(self.dict_months.keys()) and len(word.split()) == 2:
            return ['MONTH', word.split()[0], word.split()[1]]
            # index day month year
        elif word.split()[1] in list(self.dict_months.keys()) and len(word.split()) == 3:
            return ['YEAR', word.split()[0], word.split()[1], word.split()[2]]

    def format(self ,word):
        if re.search("K", word):
            return Decimal(word.replace('K', ''))*1000
        elif re.search("M", word):
            return Decimal(word.replace('M', ''))*1000000
        else:
            return word

    def check_exists_by_xpath(self, xpath):
        try:
            self.post.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return False
        return True
